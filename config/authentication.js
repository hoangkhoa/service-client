module.exports = (app) => {
    app.service.user = {
        auth: async (username, password) => {
            let data = await app.service.post('/api/token/login', { username, password });
            if (data && data.error) {
                console.error('Login error:', data.error);
                data = null;
            }
            return data ? data.user : null;
        },

        logout: async (user) => await app.service.post('/api/token/logout', { token: user.token }),

        get: async (userInfo) => {
            let data = await app.service.post('/api/token/get-user', userInfo);
            if (data && data.error) {
                console.error('Get user error:', data.error);
                data = null;
            }
            return data ? data.user : null;
        },
    };

    app.loginUser = async (req, res) => {
        if (req.session.user) {
            res.send({ error: 'You are logged in!' });
        } else {
            const { username, password } = req.body;
            try {
                const user = await app.service.user.auth(username, password);
                if (user == null) {
                    res.send({ error: 'Thông tin đăng nhập không chính xác!' });
                } else if (user.active) {
                    const sessionUser = await app.updateSessionUser(req, user);
                    res.send({ user: sessionUser });
                } else {
                    res.send({ error: 'Tài khoản của bạn chưa được kích hoạt!' });
                }
            } catch (error) {
                res.send({ error });
            }
        }
    };

    app.logoutUser = (req, res, error) => {
        if (app.isDebug) res.clearCookie('userId');
        req.session.user = null;
        res.send({ error });
    };
};