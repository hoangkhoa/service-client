module.exports = app => {
    // Log every request to the console
    if (app.isDebug) {
        const morgan = require('morgan');
        app.use(morgan('dev'));

        // Redirect to webpack server
        app.get('/*.js', (req, res) => {
            if (req.originalUrl.endsWith('.min.js')) {
                console.log(req.originalUrl);
                res.next();
            } else {
                const http = require('http');
                http.get('http://127.0.0.1:' + (app.port + 1) + req.originalUrl, response => {
                    let data = '';
                    response.on('data', chunk => data += chunk);
                    response.on('end', () => res.send(data));
                });
            }
        });
    }

    app.use((req, res) => {
        res.status(404);

        // respond with html page
        if (req.accepts('html')) {
            return res.redirect('/404.html');
        } else if (req.accepts('json')) {
            // respond with json
            return res.send({ error: 'Not found!' });
        }
    });
};