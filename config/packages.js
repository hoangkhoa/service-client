module.exports = (app) => {
    app.url = require('url');
    app.componentModel = {};

    // Protect your app from some well-known web vulnerabilities by setting HTTP headers appropriately
    const helmet = require('helmet');
    app.use(helmet.dnsPrefetchControl());
    app.use(helmet.frameguard());
    app.use(helmet.hidePoweredBy());
    app.use(helmet.hsts());
    app.use(helmet.ieNoOpen());
    // app.use(helmet.noCache());
    app.use(helmet.xssFilter());
    // app.use(helmet.referrerPolicy());
    app.use(helmet.permittedCrossDomainPolicies());

    // Get information from html forms
    const bodyParser = require('body-parser');
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 100000 }));

    const cors = require('cors');
    app.use(cors());
    app.options('*', cors());
    // Cryptography
    app.crypt = require('bcrypt');
    app.getToken = length => Array(length).fill('~!@#$%^&*()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz').map(x => x[Math.floor(Math.random() * x.length)]).join('');
    // app.sha256 = require('crypto-js/sha256');

    // Configure session
    app.set('trust proxy', 1); // trust first proxy
    const session = require('express-session'),
        sessionOptions = {
            secret: '@KKPR*u_-_7RXeN^P5!z_2JhV-u5Dq=ZZt3hY5GwU38u?*GsTjcKSXfC24_NWM%^7!_x$z3-Ag&HtLMg!2Anpn84t$Tw9N&ucq9',
            key: 'digitizing-system-file',
            resave: false,
            saveUninitialized: true,
        };
    const sessionMiddleware = session(sessionOptions);
    app.use(sessionMiddleware);
    app.io && app.io.use((socket, next) => sessionMiddleware(socket.request, sessionOptions, next));

    // Read cookies (needed for auth)
    const cookieParser = require('cookie-parser');
    app.use(cookieParser());

    // Multi upload
    const multiparty = require('multiparty');
    app.getUploadForm = () => new multiparty.Form({ uploadDir: app.uploadPath });

    // Libraries
    require('./lib/array')(app);
    require('./lib/date')(app);
    require('./lib/fs')(app);
    require('./lib/string')(app);
};