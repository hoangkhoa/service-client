module.exports = app => {
    const checkPermissions = (req, res, next, permissions) => {
        const user = req.session.user;
        if (user) {
            if (user.permissions && user.permissions.contains(permissions)) {
                next();
            } else if (permissions.length == 0) {
                next();
            } else {
                responseError(req, res);
            }
        } else {
            responseError(req, res);
        }
    };

    const responseError = (req, res) => {
        if (req.method.toLowerCase() === 'get') { // is get method
            if (req.originalUrl.startsWith('/api')) {
                res.send({ error: req.session.user ? 'request-permissions' : 'request-login' });
            } else {
                res.redirect(req.session.user ? '/request-permissions' : '/request-login');
            }
        } else {
            res.send({ error: 'You don\'t have permission!' });
        }
    };
    const responseWithPermissions = (req, success, fail, permissions) => {
        if (req.session.user) {
            if (req.session.user.permissions && req.session.user.permissions.contains(permissions)) {
                success();
            } else {
                fail && fail();
            }
        } else if (permissions.length == 0) {
            success();
        } else {
            fail && fail();
        }
    };

    const systemPermission = [];
    const menuTree = {};
    app.permission = {
        all: () => [...systemPermission],

        tree: () => app.clone(menuTree),

        add: (...permissions) => {
            permissions.forEach(permission => {
                if (typeof permission == 'string') {
                    permission = { name: permission };
                } else if (permission.menu) {
                    if (permission.menu.parentMenu) {
                        const { index, subMenusRender } = permission.menu.parentMenu;
                        if (menuTree[index] == null) {
                            menuTree[index] = {
                                parentMenu: app.clone(permission.menu.parentMenu),
                                menus: {},
                            };
                        }
                        if (permission.menu.menus == null) {
                            menuTree[index].parentMenu.permissions = [permission.name];
                        }
                        menuTree[index].parentMenu.subMenusRender = menuTree[index].parentMenu.subMenusRender || (subMenusRender != null ? subMenusRender : true);
                    }

                    const menuTreeItem = menuTree[permission.menu.parentMenu.index],
                        submenus = permission.menu.menus;
                    if (submenus) {
                        Object.keys(submenus).forEach(menuIndex => {
                            if (menuTreeItem.menus[menuIndex]) {
                                const menuTreItemMenus = menuTreeItem.menus[menuIndex];
                                if (menuTreItemMenus.title == submenus[menuIndex].title && menuTreItemMenus.link == submenus[menuIndex].link) {
                                    menuTreItemMenus.permissions.push(permission.name);
                                } else {
                                    console.error(`Menu index #${menuIndex} is not available!`);
                                }
                            } else {
                                menuTreeItem.menus[menuIndex] = app.clone(submenus[menuIndex], { permissions: [permission.name] });
                            }
                        });
                    }
                }

                systemPermission.includes(permission.name) || systemPermission.push(permission.name);
            });
        },

        check: (...permissions) => async (req, res, next) => {
            const userId = req.cookies.userId;
            if (app.isDebug && !req.session.user) {
                try {
                    const user = await app.service.user.get({ _id: userId });
                    if (user == null) {
                        res.redirect(req.session.user ? '/request-permissions' : '/request-login');
                    } else {
                        await app.updateSessionUser(req, user);
                        checkPermissions(req, res, next, permissions);
                    }
                } catch (error) {
                    res.redirect(req.session.user ? '/request-permissions' : '/request-login');
                }
            } else {
                checkPermissions(req, res, next, permissions);
            }
        },

        has: async (req, success, fail, ...permissions) => {
            if (typeof fail == 'string') {
                permissions.unshift(fail);
                fail = null;
            }
            const userId = req.cookies.userId;
            if (app.isDebug && !req.session.user && userId) {
                try {
                    const user = await app.service.user.get({ _id: userId });
                    if (!user) {
                        fail && fail({ error: 'System has errors!' });
                    } else {
                        await app.updateSessionUser(req, user);
                        responseWithPermissions(req, success, fail, permissions);
                    }
                } catch (error) {
                    fail && fail({ error: 'System has errors!' });
                }
            } else {
                responseWithPermissions(req, success, fail, permissions);
            }
        },

        getTreeMenuText: () => {
            let result = '';
            Object.keys(menuTree).sort().forEach(parentIndex => {
                result += `${parentIndex}. ${menuTree[parentIndex].parentMenu.title} (${menuTree[parentIndex].parentMenu.link})\n`;

                Object.keys(menuTree[parentIndex].menus).sort().forEach(menuIndex => {
                    const submenu = menuTree[parentIndex].menus[menuIndex];
                    result += `\t${menuIndex} - ${submenu.title} (${submenu.link})\n`;
                });
            });
            app.fs.writeFileSync(app.path.join(app.assetPath, 'menu.txt'), result);
        }
    };

    const hasPermission = (userPermissions, menuPermissions) => {
        for (let i = 0; i < menuPermissions.length; i++) {
            if (userPermissions.includes(menuPermissions[i])) return true;
        }
        return false;
    };

    app.updateSessionUser = async (req, user) => {
        user = app.clone(user, { permissions: [], menu: {} });
        delete user.password;
        try {
            for (let i = 0; i < (user.roles || []).length; i++) {
                let role = user.roles[i];
                if (role.name == 'admin') {
                    user.permissions = app.permission.all();
                    break;
                }
                (role.permission || []).forEach(permission => app.permissionHooks.pushUserPermission(user, permission.trim()));
            }
            // Add login permission => user.active == true => user:login
            if (user.active) app.permissionHooks.pushUserPermission(user, 'user:login');

            // Build menu tree
            user.menu = app.permission.tree();
            Object.keys(user.menu).forEach(parentMenuIndex => {
                let flag = true;
                const menuItem = user.menu[parentMenuIndex];
                if (menuItem.parentMenu && menuItem.parentMenu.permissions) {
                    if (hasPermission(user.permissions, menuItem.parentMenu.permissions)) {
                        delete menuItem.parentMenu.permissions;
                    } else {
                        delete user.menu[parentMenuIndex];
                        flag = false;
                    }
                }

                flag && Object.keys(menuItem.menus).forEach(menuIndex => {
                    const menu = menuItem.menus[menuIndex];
                    if (hasPermission(user.permissions, menu.permissions)) {
                        delete menu.permissions;
                    } else {
                        delete menuItem.menus[menuIndex];
                        if (Object.keys(menuItem.menus).length == 0) delete user.menu[parentMenuIndex];
                    }
                });
            });

            if (req) {
                if (req.session) {
                    req.session.user = user;
                    req.session.save();
                } else {
                    req.session = { user };
                }
            }

            return user;
        } catch (error) {
            console.error('app.updateSessionUser', error);
        }
    };

    // Permission Hook -------------------------------------------------------------------------------------------------
    const permissionHookContainer = {};
    app.permissionHooks = {
        add: (type, name, hook) => {
            if (permissionHookContainer[type]) {
                permissionHookContainer[type][name] = hook;
            } else {
                console.log(`Invalid hook type (${type})!`);
            }
        },
        remove: (type, name) => {
            if (permissionHookContainer[type] && permissionHookContainer[type][name]) {
                delete permissionHookContainer[type][name];
            }
        },

        run: async (type, user, role) => {
            const hookContainer = permissionHookContainer[type], hookKeys = Object.keys(hookContainer || {});
            for (const hookKey of hookKeys) {
                await hookContainer[hookKey](user, role);
            }
        },

        pushUserPermission: function () {
            if (arguments.length >= 1) {
                const user = arguments[0];
                for (let i = 1; i < arguments.length; i++) {
                    const permission = arguments[i];
                    if (!user.permissions.includes(permission)) user.permissions.push(permission);
                }
            }
        },
    };
};