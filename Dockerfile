# build environemnt
FROM node:14.18.1-alpine as build

# setting environment
ENV HOST="127.0.0.1"
ENV PORT="5800"
ENV SERVICE_DASHBOARD_PORT="5802"
ENV SERVICE_DASHBOARD_HOST="127.0.0.1"

# working phase
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
RUN mkdir /app/scanner
RUN chmod 777 -R /app/scanner

#COPY package-lock.json ./
RUN npm install --silent
RUN npm install -g npm@9.7.2 --silent
# RUN npm i --save-exact react-scripts@4.0.3
# RUN npm i --save-dev react-error-overlay@6.0.9
# RUN npx npm-force-resolutions
COPY . ./
RUN npm run webpack-server
RUN npm rebuild node-sass
RUN npm run build-webpack
RUN apk add curl


# production environment
EXPOSE ${PORT}
CMD ["npm", "run", "nodemon"]