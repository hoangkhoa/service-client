import React from 'react';
const backgroundDropStyle = {
    backgroundColor: '#F5F5F5',
    padding: '15px 15px 15px 15px',
    border: '1px solid #009688',
    borderRadius: '5px',
    minHeight: '65vh',
};
export default class BackgroundDrop extends React.Component {
    state = { isUploading: false, userData: null, isDragIn: false };
    box = React.createRef();
    uploadInput = React.createRef();
    listItem = [];
    setData = (userData) => this.setState({ userData })

    onDrop = (event) => {
        $(this.box.current).css('background-color', '#F6F6F6');
        if (!this.state.isDragIn) {
            return;
        }
        event.preventDefault();
        if (event.dataTransfer.items) {
            if (event.dataTransfer.items.length > 0) {
                const files = event.dataTransfer.items;
                this.onUploadFile(files);
            }
            event.dataTransfer.items.clear();
        } else {
            if (event.dataTransfer.files.length > 0) {
                this.onUploadFile(event.dataTransfer.files[0]);
            }
            event.dataTransfer.clearData();
        }
    }



    onDragOver = (event) => event.preventDefault();

    onDragEnter = (event) => {
        $(this.box.current).css({ 'background-color': '#009688' });
        this.setState({ isDragIn: true })
        ;
        event.preventDefault();
    }

    onDragLeave = (event) => {
        $(this.box.current).css({ 'background-color': '#F6F6F6' });
        this.setState({ isDragIn: false });
        event.preventDefault();
    }

    onSelectFileChanged = (event) => {
        if (event.target.files.length > 0) {
            this.onUploadFile(event.target.files[0]);
            event.target.value = '';
        }
    };

    onUploadFile = (files) => {
        this.setState({ isUploading: true });

        const formData = new FormData();
        formData.append('token', this.props.token);
        formData.append('parentPath', this.props.uploadPath ? this.props.uploadPath : '/');
        console.log(files);
        if (files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                if (files[i].kind == 'file') {
                    formData.append('files', files[i].getAsFile());
                }
            }
        }
        $.ajax({
            method: 'POST',
            url: this.props.postUrl,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            xhr: () => {
                const xhr = new window.XMLHttpRequest();
                return xhr;
            },
            complete: () => {
                if (this.props.complete) this.props.complete();
                this.props.reloadData();
            },
            success: data => {
                if (this.props.success) this.props.success(data);
                this.props.reloadData();
            },
            error: error => {
                this.setState({ isUploading: false });
                if (this.props.error) this.props.error(error);
            }
        });
    }
    render() {
        const fileAttrs = { type: 'file' };
        if (this.props.accept) fileAttrs.accept = this.props.accept;
        return (
            <div style={this.props.style} className={this.props.className}>
                <div ref={this.box} id={this.props.uploadType} style={backgroundDropStyle}
                    onDrop={this.onDrop}
                    onDragOver={this.onDragOver} onDragEnter={this.onDragEnter} onDragLeave={this.onDragLeave}>
                    <div>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }

}
