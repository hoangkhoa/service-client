import React from 'react';
import { connect } from 'react-redux';
import { updateSystemState, logout } from 'modules/_default/_init/redux';

class AdminHeader extends React.Component {
    componentDidMount() {
        T.showSearchBox = (onSearchHide = null) => {
            this.searchBox && $(this.searchBox).parent().css('display', 'flex');
            this.advancedSearch && $(this.advancedSearch).css('display', onSearchHide ? 'flex' : 'none');
            if (onSearchHide && typeof onSearchHide == 'function') {
                T.onAdvanceSearchHide = onSearchHide;
            } else {
                T.onAdvanceSearchHide = null;
            }
        };
        T.setTextSearchBox = (searchText) => {
            if (typeof searchText == 'object') searchText = '';
            this.searchBox && $(this.searchBox).val(searchText) && $(this.searchBox).trigger('change');
        };
        T.hideSearchBox = () => {
            this.searchBox && $(this.searchBox).parent().css('display', 'none');
            this.advancedSearch && $(this.advancedSearch).css('display', 'none');
        };
        T.clearSearchBox = () => {
            if (this.searchBox) this.searchBox.value = '';
        };
    }

    logout = (e) => e.preventDefault() || this.props.logout();

    search = (e) => e.preventDefault() || T.onSearch && T.onSearch(this.searchBox.value);

    onAdvanceSearch = (e) => {
        e.preventDefault();
        if ($('.app-advance-search')) {
            // Close advance search
            if ($('.app-advance-search').hasClass('show')) {
                T.onAdvanceSearchHide && T.onAdvanceSearchHide();
            }

            $('.app-advance-search').toggleClass('show');
        }
    }

    render() {
        return <>
            <header className='app-header' >
                <a className='app-header__logo' href='/'>Digitizing System</a>
                {/* <a className='app-sidebar__toggle' href='#' data-toggle='sidebar' aria-label='Hide Sidebar' /> */}
                <ul className='app-nav'>
                    <li className='app-search' style={{ display: 'none' }}>
                        <input ref={e => this.searchBox = e} className='app-search__input' type='search' placeholder='Tìm kiếm' onKeyUp={e => e.keyCode == 13 && this.search(e)} />
                        <button className='app-search__button' onClick={this.search}><i className='fa fa-search' /></button>
                    </li>
                    <li ref={e => this.advancedSearch = e} style={{ display: 'none' }} onClick={this.onAdvanceSearch}>
                        <a className='app-nav__item' href='#'>
                            <i className='fa fa-search-plus fa-lg' />
                        </a>
                    </li>
                    <li>
                        <a className='app-nav__item' href='#' onClick={e => e.preventDefault() || alert('Thư mục gốc: TODO.')}>
                            <i className='fa fa-folder fa-lg' />
                        </a>
                    </li>
                    <li>
                        <a className='app-nav__item' href='#' onClick={this.logout}>
                            <i className='fa fa-power-off fa-lg' style={{ color: 'red' }} />
                        </a>
                    </li>
                </ul>
            </header>
        </>;
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = { updateSystemState, logout };
export default connect(mapStateToProps, mapActionsToProps)(AdminHeader);