import React from 'react';
const iconTypeTable = {
    '.pdf': 'file-pdf-o',
    '.xls': 'file-excel-o',
    '.xlsx': 'file-excel-o',
    '.doc': 'file-word-o',
    '.docx': 'file-word-o',
    '.odt': 'file-word-o',
    '.zip': 'file-archive-o',
    '.rar': 'file-archive-o',
    '.tar': 'file-archive-o',
    '.png': 'file-image-o',
    '.jpg': 'file-image-o',
    '.jpeg': 'file-image-o',
    '.bmp': 'file-image-o',
    '.mp3': 'file-audio-o',
    '.mp4': 'file-movie-o',
};
export default class FolderDropBox extends React.Component {
    state = { isUploading: false, userData: null, };
    box = React.createRef();
    uploadInput = React.createRef();
    folderIcon = React.createRef();

    setData = (userData) => this.setState({ userData })

    onDrop = (event) => {
        this.folderIcon.current.classList.contains('open') ?
            this.folderIcon.current.classList.toggle('fa-folder')
            : this.folderIcon.current.classList.toggle('fa-folder-open');
        this.box.current.style.border = '1.5px solid #009688';
        event.preventDefault();
        if (event.dataTransfer.items) {
            if (event.dataTransfer.items.length > 0) {
                const files = event.dataTransfer.items;
                this.onUploadFile(files);
                this.box.current.style.border = '1.5px solid #b9eff7';
            }
            event.dataTransfer.items.clear();
        } else {
            if (event.dataTransfer.files.length > 0) {
                this.onUploadFile(event.dataTransfer.files[0]);
                this.box.current.style.border = '1.5px solid #b9eff7';
            }
            event.dataTransfer.clearData();
        }
    }
    onDragOver = (event) => event.preventDefault();

    onDragEnter = (event) => {
        this.folderIcon.current.classList.contains('open') ?
            this.folderIcon.current.classList.toggle('fa-folder')
            : this.folderIcon.current.classList.toggle('fa-folder-open');

        this.box.current.style.border = '1.5px solid #009688';
        event.preventDefault();
    }

    onDragLeave = (event) => {
        this.folderIcon.current.classList.contains('open') ?
            this.folderIcon.current.classList.toggle('fa-folder')
            : this.folderIcon.current.classList.toggle('fa-folder-open');
        this.box.current.style.border = '1.5px solid #b9eff7';
        event.preventDefault();
    }

    onSelectFileChanged = (event) => {
        if (event.target.files.length > 0) {
            this.onUploadFile(event.target.files[0]);
            event.target.value = '';
        }
    };

    onUploadFile = (files) => {
        const formData = new FormData();
        formData.append('token', this.props.token);
        let uploadPath = this.props.parenPath ? this.props.parenPath + '/' + this.props.itemName : '/' + this.props.itemName;
        formData.append('parentPath', uploadPath);
        console.log(files);
        if (files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                if (files[i].kind == 'file') {
                    formData.append('files', files[i].getAsFile());
                }
            }
        }
        $.ajax({
            method: 'POST',
            url: this.props.postUrl,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            xhr: () => {
                const xhr = new window.XMLHttpRequest();
                return xhr;
            },
            complete: () => {
                if (this.props.complete) this.props.complete();
            },
            success: data => {
                if (this.props.success) this.props.success(data);
            },
            error: error => {
                this.setState({ isUploading: false });
                if (this.props.error) this.props.error(error);
            }
        });
    }

    getIconWithType = (itemType) => {
        if(iconTypeTable[itemType]){
            return iconTypeTable[itemType];
        } else {
            return 'file-o';
        }
    }

    getColorWithLanguague = (itemLanguage) =>{
        if(itemLanguage == 'vie'){
            return 'danger';
        } else if (itemLanguage == 'eng'){
            return 'info';
        } else if (itemLanguage == 'jpn') {
            return 'warning';
        } else return 'primary';
    }

    render() {
        if (this.props.itemType == 'directory') {
            return (
                <div className={'widget-small coloured-icon ' + this.getColorWithLanguague(this.props.itemLanguage)} onDrop={this.onDrop}
                    onDragOver={this.onDragOver} onDragEnter={this.onDragEnter} onDragLeave={this.onDragLeave}
                    ref={this.box} style={{ border: '1px solid #b9eff7' }}>
                    <i className={'icon fa fa-3x fa-folder'} ref={this.folderIcon} />
                    <div className='info'>
                        <h4 style={{wordBreak: 'break-word'}}>{this.props.itemName}</h4>
                        <i className='fa fa-lg fa-trash button' style={{ cursor: 'pointer', color: 'red', position: 'absolute', top: 12, right: 24 }} onClick={this.props.deleteFunc} />
                        <i className='fa fa-lg fa-download  button' style={{ cursor: 'pointer', color: 'green', position: 'absolute', top: 42, right: 24 }} onClick={this.props.downloadFunc} />
                    </div>
                </div>
            );
        } else {
            return (
                <div className={'widget-small coloured-icon primary'}>
                    <i className={'icon fa fa-3x fa-' + this.getIconWithType(this.props.itemType)} />
                    <div className='info'>
                        <h4 style={{wordBreak: 'break-word'}}>{this.props.itemName}</h4>
                        <i className='fa fa-lg fa-trash button' style={{ cursor: 'pointer', color: 'red', position: 'absolute', top: 12, right: 24 }} onClick={this.props.deleteFunc} />
                    </div>
                </div>
            );
        }
    }
}
