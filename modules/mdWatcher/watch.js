module.exports = (app) => {
    const chokidar = require('chokidar');
    const addFileServer = async (watchFolderPath, localPath, token) => {
        const FormData = require('form-data');
        const formData = new FormData();
        const folderScanner = app.path.basename(watchFolderPath);
        const serverPath = app.path.join(folderScanner, localPath.replace(watchFolderPath, ''));
        const itemName = app.path.basename(localPath);
        let res = '';
        if (itemName == '.gitignore') {
            return res;
        }
        formData.append('token', token);
        formData.append('parentPath', app.path.dirname(serverPath));
        formData.append('files', app.fs.readFileSync(localPath), itemName);
        const config = {
            headers: {
                ...formData.getHeaders(),
            },
        };
        try {
            res = await app.service.post('/api/file', formData, config);
        } catch (error) {
            console.error(error);
        }
        app.io.emit('upload-item', serverPath);
        return res;
    };
    const createFolderServer = async (watchFolderPath, localPath, language, token) => {
        const folderScanner = app.path.basename(watchFolderPath);
        const serverPath = app.path.join(folderScanner, localPath.replace(watchFolderPath, ''));
        const data = { token: token, parentPath: app.path.dirname(serverPath), language: language, itemName: app.path.basename(localPath) };
        let res = '';
        try {
            res = await app.service.post('/api/folder/create', data);
        } catch (error) {
            console.error(error);
        }
        app.io.emit('upload-item', serverPath);
        return res;
    };

    const readFolderAttributeFile = (watchFolderPath, targetPath) => {
        let targetItem = '';
        let data = '';
        while (watchFolderPath != targetPath) {
            targetItem = app.path.join(targetPath, '.config');
            try {
                data = app.fs.readFileSync(targetItem, 'utf8');
                return data;
            }
            catch {
                targetPath = app.path.resolve(targetPath, '..');
            }
        }
        return data;
    };
    app.startWatcher = async (watchFolderPath) => {
        const token = await app.watcherToken;
        await app.createFolder(app.path.join(watchFolderPath, '/TiengViet'), app.path.join(watchFolderPath, '/English'), app.path.join(watchFolderPath, '/Japanese'));
        await app.fs.writeFileSync(app.path.join(watchFolderPath, '/TiengViet', '.config'), 'vie');
        await app.fs.writeFileSync(app.path.join(watchFolderPath, '/English', '.config'), 'eng');
        await app.fs.writeFileSync(app.path.join(watchFolderPath, '/Japanese', '.config'), 'jpn');
        const watcher = chokidar.watch(watchFolderPath.toString(), {
            ignored: /(^|[\/\\])\../,
            persistent: true,
            awaitWriteFinish: true
        });
        watcher
            .on('add', function async(path) {
                const res = addFileServer(watchFolderPath, path, token);
                if (res.error) {
                    if (res.error == 'TokenExpiredError') {
                        try {
                            app.watcherToken = app.getWatcherToken();
                            addFileServer(watchFolderPath, path, token);
                        } catch (error) {
                            console.error(error);
                        }
                    } else {
                        console.error('watcher add error');
                    }
                }
            })
            .on('addDir', function async(path) {
                const language = readFolderAttributeFile(watchFolderPath, path);
                const res = createFolderServer(watchFolderPath, path, language, token);
                if (res.error) {
                    if (res.error == 'TokenExpiredError') {
                        try {
                            app.watcherToken = app.getWatcherToken();
                            createFolderServer(watchFolderPath, path, token);
                        } catch (error) {
                            console.error(error);
                        }
                    } else {
                        console.error('watcher addDir error');
                    }
                }
            })
            .on('error', error => console.error(`Watcher error: ${error}`));
    };
};
