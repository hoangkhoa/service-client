module.exports = (app) => {
    const getSecret = async () => {
        let data = '';
        try {
            data = await app.fs.readFileSync(__dirname + '/secret.txt', 'utf8');
            return data;
        } catch (error) {
            console.error(`Get secret error: ${error}`);
        }
        return data;
    };
    app.getWatcherToken = async () => {
        const secret = await getSecret();
        let data = '';
        let token = '';
        try {
            data = await app.service.get('/api/watcher/token', {secret: secret});
            if(data.token){token = data.token;}
        } catch (error) {
            console.error(`Get watcher token error: ${error}`);
        }
        return token;
    };
};