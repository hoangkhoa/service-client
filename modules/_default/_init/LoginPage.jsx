import React from 'react';
import { connect } from 'react-redux';
import { login } from './redux';

class LoginPage extends React.Component {
    state = {};
    componentDidMount() {
        $(this.txtEmail).focus();
        T.ready(() => {
            const { user } = this.props.system ? this.props.system : {};
            if (user) {
                window.location.pathname = '/user';
            }

            $('.login-content [data-toggle="flip"]').click(function () {
                $('.login-box').toggleClass('flipped');
                return false;
            });
        });
    }

    onSubmit = e => {
        e.preventDefault();
        const user = this.props.system && this.props.system.user ? this.props.system.user : null;
        let btnSend = $(this.btnSend).attr('disabled', true),
            errorMessage = $(this.errorMessage),
            data = {
                username: this.txtEmail.value.trim(),
                password: this.txtPassword.value
            };
        if (!user) {
            if (data.username === '') {
                btnSend.attr('disabled', false);
                errorMessage.html('Your email is empty!');
                $(this.txtEmail).focus();
            } else if (data.password === '') {
                btnSend.attr('disabled', false);
                errorMessage.html('Your password is empty');
                $(this.txtPassword).focus();
            } else {
                this.props.login(data, result => {
                    btnSend.attr('disabled', false);
                    if (result.error) {
                        errorMessage.html(result.error);
                    } else {
                        window.location.pathname = '/user';
                    }
                });
            }
        }
    }

    render() {
        $('header').css('display', 'none');
        $('.app-sidebar').css('display', 'none');
        return (
            <div>
                <section className='material-half-bg'>
                    <div className='cover' />
                </section>
                <section className='login-content'>
                    <div className='logo text-center'>
                        <h1>Digitizing System</h1>
                        <h4 id='activeMessage' />
                    </div>
                    <div className='login-box'>
                        <form className='login-form' onSubmit={this.onSubmit}>
                            <h3 className='login-head'><i className='fa fa-lg fa-fw fa-user' />Đăng nhập</h3>
                            <div className='form-group'>
                                <label className='control-label'>Email</label>
                                <input ref={e => this.txtEmail = e} className='form-control' type='text' placeholder='Email' autoFocus />
                            </div>
                            <div className='form-group'>
                                <label className='control-label'>Mật khẩu</label>
                                <input ref={e => this.txtPassword = e} className='form-control' type='password' placeholder='Mật khẩu' />
                            </div>
                            <div className='form-group btn-container'>
                                <button className='btn btn-primary btn-block' ref={e => this.btnSend = e}>
                                    <i className='fa fa-sign-in fa-lg fa-fw' />Đăng nhập
                                </button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({ system: state.system });
const mapActionsToProps = { login };
export default connect(mapStateToProps, mapActionsToProps)(LoginPage);