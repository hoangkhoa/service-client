module.exports = (app) => {
    // Routes ---------------------------------------------------------------------------------------------------------------------------------------
    app.permission.add(
        { name: 'user:login', menu: { parentMenu: app.parentMenu.user } },
    );

    ['/index.htm(l)?', '/404.htm(l)?', '/request-permissions(/:roleId?)', '/request-login'].forEach((route) => app.get(route, app.templates.admin));
    app.post('/login', app.loginUser);
    app.post('/logout', (req, res) => app.logoutUser(req, res, null));

    // API ------------------------------------------------------------------------------------------------------------------------------------------
    app.get('/api/state', (req, res) => {
        const data = {};
        data.isDebug = app.isDebug ? true : false;
        if (req.session.user) data.user = req.session.user;
        data.ocrUrl = app.ocrUrl;
        res.send(data);
    });
    app.createFolder(app.assetPath, app.path.join(app.assetPath, '/upload'), app.path.join(app.assetPath, '/temp'));

    // Upload ---------------------------------------------------------------------------------------------------------------------------------------
    app.post('/user/upload', app.permission.check('user:login'), (req, res) => {
        app.getUploadForm().parse(req, (error, fields, files) => {
            console.log('User Upload:', fields, files, req.query);

            if (error) {
                res.send({ error });
            } else {
                let hasResponse = false;
                app.uploadHooks.run(req, fields, files, req.query, (data) => {
                    if (hasResponse == false) res.send(data);
                    hasResponse = true;
                });
            }
        });
    });

    app.uploadImage = (dataName, getItem, _id, srcPath, done) => {
        if (_id == 'new') { // Upload hình ảnh khi chưa có tạo đối tượng trong database
            const dateFolderName = app.date.getDateFolderName(),
                image = app.path.join('/temp', dateFolderName, 'img', dataName, app.path.basename(srcPath));
            app.createFolder(
                app.path.join(app.assetPath, '/temp', dateFolderName),
                app.path.join(app.assetPath, '/temp', dateFolderName, 'img'),
                app.path.join(app.assetPath, '/temp', dateFolderName, 'img', dataName));
            app.fs.rename(srcPath, app.path.join(app.assetPath, image), (error) => done({ error, image }));
        } else { // Cập nhật image vào item
            const image = '/img/' + dataName + '/' + _id + app.path.extname(srcPath),
                destPath = app.path.join(app.publicPath, image);
            getItem(_id, (error, item) => {
                if (error || item == null) {
                    done({ error: error || 'Invalid Id!' });
                } else {
                    if (srcPath.startsWith('/temp/') || srcPath.startsWith('\\temp\\')) {
                        srcPath = app.path.join(app.assetPath, srcPath);
                    } else {
                        app.deleteImage(item.image); // Xoá hình cũ
                    }
                    app.fs.rename(srcPath, destPath, (error) => {
                        if (error) {
                            done({ error });
                        } else {
                            item.image = image + '?t=' + new Date().getTime().toString().slice(-8);
                            item.save((error) => done({ error, item, image: item.image }));
                        }
                    });
                }
            });
        }
    };
};