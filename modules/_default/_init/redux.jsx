import T from 'view/js/common';

const SystemUpdateState = 'SystemUpdateState';

export default function systemReducer(state = null, data) {
    switch (data.type) {
        case SystemUpdateState:
            return Object.assign({}, state, data.state);

        default:
            return state;
    }
}

// Action -------------------------------------------------------------------------------------------------------------
export function getSystemState(done) {
    return dispatch => {
        const url = '/api/state';
        T.get(url, data => {
            data && dispatch({ type: SystemUpdateState, state: data });
            done && done(data);
        }, error => {
            console.error(error);
            T.notify('Lấy thông tin hệ thống lỗi!', 'danger');
            done && done();
        });
    };
}

export function login(data, done) {
    return () => {
        T.post('/login', data, res => {
            if (res.error) {
                done({ error: res.error ? res.error : '' });
            } else {
                done({ user: res.user });
            }
        }, error => {
            console.error(error);
            done({ error: 'Đăng nhập thất bại!' });
        });
    };
}

export function logout(config) {
    if (config == undefined) config = {};
    if (config.title == undefined) config.title = 'Đăng xuất';
    if (config.message == undefined) config.message = 'Bạn có muốn đăng xuất không?';
    if (config.errorMessage == undefined) config.errorMessage = 'Đăng xuất thất bại!';
    return dispatch => {
        T.confirm(config.title, config.message, true, isConfirm => {
            isConfirm && T.post('/logout', {},
                () => {
                    dispatch({ type: SystemUpdateState, state: { user: null } });
                    const pathname = window.location.pathname;
                    if (pathname.startsWith('/user')) {
                        window.location = '/';
                    } else if (config.done) {
                        config.done();
                    }
                },
                error => console.error(error) || T.notify(config.errorMessage, 'danger')
            );
        });
    };
}


// AJAX ---------------------------------------------------------------------------------------------------------------
export function updateSystemState(state) {
    return { type: SystemUpdateState, state };
}
