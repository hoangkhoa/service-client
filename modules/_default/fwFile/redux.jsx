import T from 'view/js/common';

// Reducer ------------------------------------------------------------------------------------------------------------
const FileGetChildren = 'FileGetChildren';

export default function fileReducer(state = {}, data) {
    switch (data.type) {
        case FileGetChildren:
            return { path: data.path, items: data.items };
        default:
            return state;
    }
}

// Actions ------------------------------------------------------------------------------------------------------------
export function getFileChildren(path, done) {
    return dispatch => {
        const url = '/api/file/items';
        T.get(url, { path }, data => {
            if (data.error == 'TokenExpiredError') {
                window.location = '/';
            } else if (data.error) {
                T.notify('Lấy tập tin bị lỗi!', 'danger');
                console.error('GET:', url, '=>', data.error);
            } else {
                dispatch({ type: FileGetChildren, path, items: data.items });
                done && done(data);
            }
        }, error => console.error(error) || T.notify('Lấy tập tin bị lỗi!', 'danger'));
    };
}

export function deleteFileItem(parentPath, itemName, done) {
    return dispatch => {
        const url = '/api/file';
        T.delete(url, { parentPath, itemName }, data => {
            if (data.error == 'TokenExpiredError') {
                window.location = '/';
            } else if (data.error) {
                T.notify('Xóa dữ liệu bị lỗi!', 'danger');
                console.error('DELETE:', url, '=>', data.error);
            } else {
                done && done(data);
                T.alert('Dữ liệu được xóa thành công!', 'error', false, 800);
                dispatch({ type: FileGetChildren, path: parentPath, items: data.items });
            }
        }, error => console.error(error) || T.notify('Xóa dữ liệu bị lỗi!', 'danger'));
    };
}

export function createFolderItem(parentPath, itemName, language, done) {
    return dispatch => {
        const url = '/api/folder/create';
        T.post(url, { parentPath, itemName, language }, data => {
            if (data.error == 'TokenExpiredError') {
                window.location = '/';
            } else if (data.error) {
                T.notify('Tạo thư mục bị lỗi!', 'danger');
                console.error('POST:', url, '=>', data.error);
            } else {
                done && done(data);
                T.alert('Tạo thư mục thành công!', 'success', false, 800);
                dispatch({ type: FileGetChildren, path: parentPath, items: data.items });
            }
        }, error => console.error(error) || T.notify('Xóa dữ liệu bị lỗi!', 'danger'));
    };
}

export function uploadFileItem(formData, done) {
    const url = '/api/file';
    return T.post(url, formData, data => {
        if (data.error == 'TokenExpiredError') {
            window.location = '/';
        } else if (data.error) {
            T.notify('Tải lên tài liệu bị lỗi!', 'danger');
            console.error('POST:', url, '=>', data.error);
        } else {
            done && done(data);
            T.alert('Tải thành công!', 'success', false, 800);
        }
    }, error => console.error(error) || T.notify('Up dữ liệu bị lỗi!', 'danger'));
}
