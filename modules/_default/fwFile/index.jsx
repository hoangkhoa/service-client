//TEMPLATES: admin
import Loadable from 'react-loadable';
import Loading from 'view/component/Loading';

import file from './redux';

export default {
    redux: {
        file,
    },
    routes: [
        {
            path: '/user',
            component: Loadable({ loading: Loading, loader: () => import('./userPage') })
        },
    ],
};
