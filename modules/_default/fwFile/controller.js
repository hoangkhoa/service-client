module.exports = app => {
    app.get('/api/file/download/*', app.permission.check('user:login'), async (req, res) => {
        const path = app.url.parse(req.url).pathname.substring('/api/file/download'.length);
        //let url = (app.isDebug ? 'http://localhost:' + (app.port + 2) : app.ocrUrl) + '/api/file/download' + '?t=' + new Date().getTime();
        let url = app.ocrUrl  + '/api/file/download' + '?t=' + new Date().getTime();
        let data = { path, token: req.session.user.token};
        const params = new URLSearchParams(data).toString();
        if (params.length) url = url + (url.includes('?') ? '&' : '?') + params;
        const request = require('request');
        try{
            request(url).pipe(res);
        }catch(e){
            res.send(e);
        }
    });

    app.get('/api/file/items', app.permission.check('user:login'), async (req, res) => {
        const { path } = req.query;
        const data = await app.service.get('/api/file/items', { path, token: req.session.user.token });
        if (!data) {
            res.send({ error: 'Error when read files!' });
        } else if (!data.error) {
            res.send({ items: data.items || [] });
        } else if (data.error == 'TokenExpiredError') {
            app.logoutUser(req, res, data.error);
        } else {
            res.send({ error: data.error });
        }
    });

    app.delete('/api/file', app.permission.check('user:login'), async (req, res) => {
        const { parentPath, itemName } = req.body;
        const data = await app.service.delete('/api/file', { parentPath, itemName, token: req.session.user.token });
        if (data && !data.error) {
            res.send({ items: data.items || [] });
        } else {
            res.send({ error: 'Error when delete item!' });
        }
    });

    app.post('/api/folder/create', app.permission.check('user:login'), async (req, res) =>{
        const { parentPath, itemName, language } = req.body;
        const data = await app.service.post('/api/folder/create', { parentPath, itemName, language, token: req.session.user.token });
        if (data && !data.error) {
            res.send({ items: data.items || [] });
        } else {
            res.send({ error: 'Error when delete item!' });
        }
    });

    app.get('/api/folder/download/*', app.permission.check('user:login'), async (req, res) => {
        const path = app.url.parse(req.url).pathname.substring('/api/folder/download'.length);
        //let url = (app.isDebug ? 'http://localhost:' + (app.port + 2) : app.ocrUrl) + '/api/folder/download' + '?t=' + new Date().getTime();
        let url = app.ocrUrl + '/api/folder/download' + '?t=' + new Date().getTime();
        let data = { path, token: req.session.user.token};
        const params = new URLSearchParams(data).toString();
        if (params.length) url = url + (url.includes('?') ? '&' : '?') + params;
        const request = require('request');
        try{
            request(url).pipe(res);
        }catch(e){
            res.send(e);
        }
    });

    
};