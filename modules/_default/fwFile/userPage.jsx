import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { AdminPage, AdminModal, FormTextBox, FormSelect } from 'view/component/AdminPage';
import BackgroundDrop from 'view/component/BackgroundDrop';
import FolderDropBox from 'view/component/FolderDropBox';
import T from 'view/js/common';
import { getFileChildren, deleteFileItem, createFolderItem } from './redux';

const languageSelector = [
    {  id: 'eng', text:'English'},
    {  id: 'vie', text:'Tiếng Việt (Vietnamese)'},
    {  id: 'jpn', text:'にほんご (Japanese)'},
];

class FolderModal extends AdminModal {
    componentDidMount() {
        $(document).ready(() => this.onShown(() => this.folderName.focus()));
    }

    onShow = () => {
        this.folderName.value('');
    }

    onSubmit = () => {
        const folderName = this.folderName.value().trim();
        const parentPath = this.props.parentPath == '' ? '/' : this.props.parentPath;
        const language = this.language.value(); 
        if (folderName == '') {
            T.notify('Tên bị trống!', 'danger');
            this.folderName.focus();
        } else if (!language) {
            T.notify('Chưa chọn ngôn ngữ cho thư mục!', 'danger');
            this.folderName.focus();
        } else {
            this.props.createFolderItem(parentPath, folderName, language);
            this.hide();
        }
    }

    render = () => this.renderModal({
        title: 'Tạo thư mục',
        body: <>
            <FormTextBox ref={e => this.folderName = e} label='Tên thư mục' />
            <FormSelect ref={e =>this.language = e} label='Ngôn ngữ' data = {languageSelector}/>
        </>,
    });
}
//TODO: Drag/Drop to upload file
class UserPage extends AdminPage {
    state = { loading: true, path: '' };
    componentDidMount() {
        T.ready(() => this.loadData());
    }

    componentDidUpdate() {
        this.loadData();
    }

    reloadData = () => {
        const path = new URL(window.location.href).searchParams.get('path');
        this.props.getFileChildren(this.state.path, () => this.setState({ path, loading: false }));
    }

    loadData = () => {
        const path = new URL(window.location.href).searchParams.get('path');
        (path != this.state.path) && this.props.getFileChildren(path, () => this.setState({ path, loading: false }));
    }

    loadPath = (e, type, path) => {
        e && e.preventDefault();
        if (type == 'directory') {
            this.props.history.push('/user?' + new URLSearchParams({ path }).toString());
        } else {
            T.download('/api/file/download' + path, path.split('/').reverse()[0]);
        }
    }

    deleteFileItem = (e, parentPath, itemName) => e.stopPropagation() || T.confirm('Delete', `Bạn có chắc bạn muốn xoá ${itemName}?`, true, isConfirm =>
        isConfirm && this.props.deleteFileItem(parentPath, itemName));

    downloadFolder = (e, path) => {
        e.stopPropagation();
        T.download('/api/folder/download' + path, path.split('/').reverse()[0]);
    }

    render() {
        let { path, items } = this.props.file;
        let ocrUrl = this.props.system.ocrUrl;
        let backRoute = '';
        const pathList = [<a key={0} href='#' onClick={e => this.loadPath(e, 'directory', '/')}>Gốc</a>];

        if (path == '/') path = '';
        if (path) {
            const pathItems = path.split('/');
            for (let i = 1; i < pathItems.length; i++) {
                if (i + 1 < pathItems.length) backRoute += '/' + pathItems[i];
                let currentPath = '';
                for (let j = 1; j <= i; j++) currentPath += '/' + pathItems[j];
                pathList.push(
                    <span key={i * 2}>&nbsp;/&nbsp;</span>,
                    <a key={i * 2 + 1} href='#' onClick={e => this.loadPath(e, 'directory', currentPath)}>{pathItems[i]}</a>
                );
            }
        }
        T.socket.on('upload-item', serverPath => {
            serverPath = serverPath.replaceAll(/[\\]/ig, '/');
            if (serverPath.startsWith(this.state.path)) {
                this.reloadData();
            }
        });
        return this.renderPage(
            {
                icon: 'fa fa-folder-open-o',
                title: <>Đường dẫn: {pathList}</>,
                content: <>
                    <BackgroundDrop uploadPath={path == null ? '' : path} reloadData={this.reloadData} postUrl={ocrUrl + '/api/file'}>
                        {items && items.length ?
                            <div className='row'>
                                {items.sort((a, b) => a.type + a.name > b.type + b.name ? +1 : -1).map((item, index) =>
                                    <a key={index} href='#' onClick={e => this.loadPath(e, item.type, (path == null ? '' : path) + '/' + item.name)} className='col-md-4 col-lg-3'>
                                        <FolderDropBox itemType={item.type} parentPath={(path == null ? '' : path)} itemName={item.name} itemLanguage={item.language} deleteFunc={e => this.deleteFileItem(e, path, item.name)}
                                            downloadFunc={e => this.downloadFolder(e, (path == null ? '' : path) + '/' + item.name)} postUrl={ocrUrl + '/api/file'}>
                                        </FolderDropBox>
                                    </a>)
                                }
                            </div> :
                            <p style={{ textAlign: 'center' }} >{this.state.loading ? 'Đang tải...' : 'Thư mục trống!'}</p>
                        }
                    </BackgroundDrop>
                    <FolderModal ref={e => this.folderModal = e} parentPath={path == null ? '' : path} createFolderItem={this.props.createFolderItem} />
                </>,
                onCreate: e => e.preventDefault() || this.folderModal.show(),
                backRoute: path ? () => this.loadPath(null, 'directory', backRoute) : null,
            });
    }
}
const mapStateToProps = state => ({ system: state.system, file: state.file });
const mapActionsToProps = { getFileChildren, deleteFileItem, createFolderItem };
export default connect(mapStateToProps, mapActionsToProps)(withRouter(UserPage));