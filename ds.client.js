(async () => {
    const appConfig = require('./package.json');
    const express = require('express');
    const app = express();
    app.appName = appConfig.name;
    app.fs = require('fs');
    app.path = require('path');
    const server = require('http').createServer(app);

    // Dockerize ------------------------
    const envConfig = process.env
    app.port = envConfig.PORT
    app.rootUrl = envConfig.HOST || '127.0.0.1'
    app.ocrUrlPort = envConfig.SERVICE_DASHBOARD_PORT
    app.ocrUrl = app.ocrUrlPort ? `http://${envConfig.SERVICE_DASHBOARD_HOST}:${app.ocrUrlPort}` : `http://${envConfig.SERVICE_DASHBOARD_HOST}`
        
    // Variables ------------------------------------------------------------------------------------------------------
    app.debugUrl = `http://127.0.0.1:${app.port}`;
    app.email = appConfig.email;
    app.assetPath = app.path.join(__dirname, 'asset');
    app.viewPath = app.path.join(__dirname, 'view');
    app.modulesPath = app.path.join(__dirname, 'modules');
    app.publicPath = app.path.join(__dirname, 'public');
    app.uploadPath = app.path.join(__dirname, 'upload');
    app.faviconPath = app.path.join(__dirname, 'public/img/favicon.ico');
    app.watchFolderPath = app.path.join(__dirname,'scanner');
    // Configure ------------------------------------------------------------------------------------------------------
    await require('./config/common')(app);
    require('./config/view')(app, express);
    require('./config/io')(app, server);
    require('./config/packages')(app, server, appConfig);
    require('./config/authentication')(app);
    require('./config/permission')(app);
    require('./modules/mdWatcher/watch')(app);
    require('./modules/mdWatcher/generateWatcherToken')(app);


    // Init -----------------------------------------------------------------------------------------------------------
    app.createTemplate('admin');
    app.loadModules();
    app.get('/user', app.permission.check(), app.templates.admin);
    app.get('/', app.templates.admin);
    app.get('*', async (req, res, next) => {
        if (app.isDebug && req.session.user) await app.updateSessionUser(req, req.session.user);
        next();
    });
    
    // Launch website -------------------------------------------------------------------------------------------------
    require('./config/debug')(app);
    app.watcherToken = app.getWatcherToken();
    app.startWatcher(app.watchFolderPath);
    server.listen(app.port, () => console.log(` - The ${appConfig.title} is running on http://${app.rootUrl}:${app.port}`));
})();